const int Red = 9;
const int Green = 10;
const int Blue = 11;

void setup() {
  Serial.begin(9600);
  pinMode(Red, OUTPUT);
  pinMode(Blue, OUTPUT);
  pinMode(Green, OUTPUT);
}

void loop() {
  if(Serial.available() > 0){
    String input = Serial.readStringUntil('\n');
    input.trim();
    
    if(input == "r"){
  	  digitalWrite(Red, HIGH);
      digitalWrite(Green, LOW);
      digitalWrite(Blue, LOW);
    }
  
    else if(input == "g"){
      digitalWrite(Red, LOW);
      digitalWrite(Green, HIGH);
      digitalWrite(Blue, LOW);
    }
    
    else if(input == "b"){
      digitalWrite(Red, LOW);
      digitalWrite(Green, LOW);
      digitalWrite(Blue, HIGH);
    }
    
    else if(input == "On"){
      digitalWrite(Red, HIGH);
      digitalWrite(Green, HIGH);
      digitalWrite(Blue, HIGH);
    }
                
    else if(input == "off"){
      digitalWrite(Red, LOW);
      digitalWrite(Green, LOW);
      digitalWrite(Blue, LOW);
    }
  }
}